// /** @type {import('next').NextConfig} */
// const nextConfig = {
//   reactStrictMode: true,
//   images: {
//     domains: ['dummyimage.com'],
//   },
//   async redirects() {
//     return [
//       {
//         source: '/',
//         destination: '/products',
//         permanent: true,
//       },
//     ];
//   },
// };

// module.exports = nextConfig;

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['dummyimage.com'],
  },
};

module.exports = nextConfig;
