import Layout from '@/components/layout';
import Table from '@/components/table';
import { DataProducts } from '@/data/products';
import { useState } from 'react';

const Content = () => {
  const [Data, setData] = useState(DataProducts);

  const productColumns = [
    {
      header: 'Product name',
      key: 'name',
    },
    {
      header: 'Product Price',
      key: 'price',
    },
    {
      header: 'Description',
      key: 'description',
    },
    {
      header: 'Category',
      key: 'category',
    },
    {
      header: 'Expiry Date',
      key: 'expiryDate',
    },
  ];

  const handleDeleteProduct = (id: String) => {
    const Dummy = Data.filter((product) => {
      return product.id !== id;
    });

    setData(Dummy);
  };

  return (
    <Layout>
      <div className='bg-[#F1F5F9] h-full p-5'>
        <div className='bg-white w-full p-5 flex items-center justify-between border-b-2 border-[#F1F5F9]'>
          <p className='font-semibold'>Products</p>
          <div className='flex gap-5'>
            <div className='flex items-center justify-center gap-2'>
              <input type='checkbox' className='h-4 w-4 rounded bg-gray-100' />
              <label htmlFor='hide'>Hide expired product</label>
            </div>
            <select
              name='category'
              id='category'
              className='border rounded-md px-3 py-2'
            >
              <option value=''>All Category</option>
              <option value='ready'>Ready</option>
              <option value='pre-order'>Pre-Order</option>
            </select>
            <button className='bg-indigo-500 hover:bg-indigo-600 hover:bg-[indigo-700] text-white px-5 py-2 rounded-md active:scale-95 transition-transform'>
              Add New Product
            </button>
          </div>
        </div>
        <div className='bg-white w-full p-5'>
          <Table
            items={Data}
            columns={productColumns}
            handleDelete={handleDeleteProduct}
          />
        </div>
      </div>
    </Layout>
  );
};

export default Content;
