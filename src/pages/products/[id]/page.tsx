import { DataProducts } from '@/data/products';

const Page = ({ params }: { params: { id: string } }) => {
  console.log(params);
  
  return <div>Page</div>;
};

export async function generateStaticParams() {
  return DataProducts.map((product) => ({
    id: product.id,
  }));
}

export default Page;
