import Image from 'next/image';

interface TableProps {
  items: any[];
  columns: any[];
  handleDelete: (id: String) => void;
}

const Table = ({ items, columns, handleDelete }: TableProps) => {
  const handle = (id: String) => {
    handleDelete(id);
  };
  return (
    <div className='relative overflow-x-auto'>
      <table className='w-full text-sm text-left text-gray-500'>
        <thead className='text-xs text-gray-400 bg-slate-100 rounded-md'>
          <tr>
            {columns.map((column, index) => {
              return (
                <th
                  scope='col'
                  className='px-6 py-5 whitespace-nowrap'
                  key={index}
                >
                  {column.header}
                </th>
              );
            })}
            <th scope='col' className='px-6 py-5'>
              ACTION
            </th>
          </tr>
        </thead>
        <tbody>
          {items.map((item) => {
            return (
              <tr className='border-b-2 border-slate-100' key={item.id}>
                {columns.map((column) => {
                  return (
                    <td
                      key={column.name}
                      scope='row'
                      className={`px-6 py-4 font-medium ${
                        item.category === 'Pre-Order' && 'opacity-50'
                      }`}
                    >
                      {column.key === 'name' ? (
                        <div className='flex items-center gap-3 whitespace-nowrap'>
                          <Image
                            className='rounded-full'
                            src={item['image']}
                            alt='icon-products'
                            width={64}
                            height={64}
                          />
                          <p>{item[column.key]}</p>
                        </div>
                      ) : column.key === 'username' ? (
                        <div className='flex items-center gap-3 whitespace-nowrap'>
                          <Image
                            className='rounded-full'
                            src={item['profilePicture']}
                            alt='icon-products'
                            width={64}
                            height={64}
                          />
                          <p>{item[column.key]}</p>
                        </div>
                      ) : (
                        <p
                          className={`${
                            column.key === 'price' &&
                            "text-green-500 font-semibold before:content-['Rp']"
                          } ${
                            column.key !== 'description' && 'whitespace-nowrap'
                          }`}
                        >
                          {item[column.key]}
                        </p>
                      )}
                    </td>
                  );
                })}

                <td className='text-center'>
                  <button
                    className='px-3 py-2 rounded-md bg-red-600 hover:bg-red-700 text-white flex items-center justify-center gap-2 border-none active:scale-95 transition-transform'
                    onClick={() => handle(item.id)}
                  >
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      fill='none'
                      viewBox='0 0 24 24'
                      strokeWidth={1.5}
                      stroke='currentColor'
                      className='w-5 h-5'
                    >
                      <path
                        strokeLinecap='round'
                        strokeLinejoin='round'
                        d='M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0'
                      />
                    </svg>

                    <p>Hapus</p>
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
