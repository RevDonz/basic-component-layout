import Image from 'next/image';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

const Sidebar = () => {
  const pathname = usePathname();
  const navLinks = [
    {
      href: '/products',
      name: 'Products',
      icon: '/icon/products.svg',
    },
    {
      href: '/users',
      name: 'Users',
      icon: '/icon/users.svg',
    },
  ];

  return (
    <div className='w-96 bg-[#1E293B] text-white'>
      <div className='flex flex-col px-4 py-4'>
        <Image
          src={'/icon/Logo.svg'}
          alt='icon-products'
          width={32}
          height={32}
        />
        <p className='mt-10 text-[#64748B] font-semibold'>PAGES</p>
        <ul>
          {navLinks.map((link) => {
            const isActive = pathname.startsWith(link.href);

            return (
              <Link href={link.href} key={link.name}>
                <li
                  className={`hover:bg-[#0F172A] p-3 rounded-sm flex gap-3 my-1 ${
                    isActive ? 'bg-[#0F172A]' : 'bg-[#1E293B]'
                  }`}
                >
                  <Image
                    src={link.icon}
                    alt='icon-products'
                    width={24}
                    height={24}
                  />
                  <p>{link.name}</p>
                </li>
              </Link>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
