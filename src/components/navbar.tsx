import Image from 'next/image';

const Navbar = () => {
  return (
    <div className='bg-white h-16 flex items-center justify-end'>
      <div className='flex gap-3'>
        <div className='bg-[#F1F5F9] w-8 h-8 rounded-full flex items-center justify-center'>
          <Image
            src={'/icon/search.svg'}
            alt='icon-Users'
            width={16}
            height={16}
          />
        </div>
        <div className='bg-[#F1F5F9] w-8 h-8 rounded-full flex items-center justify-center'>
          <Image
            src={'/icon/message.svg'}
            alt='icon-Users'
            width={16}
            height={16}
          />
        </div>
        <div className='bg-[#F1F5F9] w-8 h-8 rounded-full flex items-center justify-center'>
          <Image
            src={'/icon/icon.svg'}
            alt='icon-Users'
            width={16}
            height={16}
          />
        </div>
      </div>
    </div>
  );
};

export default Navbar;
