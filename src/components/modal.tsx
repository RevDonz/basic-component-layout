const Modal = () => {
  return (
    <div className='fixed inset-0 flex items-center justify-center z-50'>
      <div className='bg-gray-800 bg-opacity-75 absolute inset-0'></div>
      <div className='bg-white rounded-lg p-6 z-10'>
        <div className='mb-4'>
          <h2 className='text-lg font-bold'>Perhatian!</h2>
        </div>
        <p>Ini adalah contoh modal alert menggunakan Tailwind CSS.</p>
        <div className='mt-6'>
          <button className='px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600'>
            Tutup
          </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
