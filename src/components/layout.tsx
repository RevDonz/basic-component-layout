import Navbar from '@/components/navbar';
import Sidebar from '@/components/sidebar';
import { Inter } from 'next/font/google';

interface LayoutProps {
  children?: React.ReactNode;
}
const inter = Inter({ subsets: ['latin'] });

const Layout = ({ children }: LayoutProps) => {
  return (
    <div className={`flex flex-row w-full min-h-screen ${inter.className}`}>
      <Sidebar />
      <div className='flex flex-col w-full'>
        <Navbar />
        {children}
      </div>
    </div>
  );
};

export default Layout;
